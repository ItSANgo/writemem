/**
 * @file writemem_main.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief write memory - main.
 * @version 0.1.0
 * @date 2020-01-02
 * 
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include "writemem.h"
#include <err.h>
#include <sysexits.h>
#include <getopt.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>


/**
 * @brief writemem main.
 * 
 * @param argc number of arguments.
 * @param argv arguments.
 * @return int 
 */
int
main(int argc, char *argv[])
{
    const char *length_arg = NULL;

    for (;;) {
        static struct option options[] = {
            { "help", 0, NULL, 'h' },
            { "length", 0, NULL, 'l' },
            { NULL, 0, NULL, 0 }
        };
        int option_index = 0;
        int c = getopt_long(argc, argv, "hl:", options, &option_index);
        if (c == -1) {
            break;
        }
        switch (c) {
        case 'h':
            help(EX_OK, stdout, argv[0]); /*NOTREACHED*/
            break;
        case 'l':
            if (length_arg) {
                warnx("--length can't duplicate!\n");
                help(EX_USAGE, stderr, argv[0]); /*NOTREACHED*/
            }
            length_arg = optarg;
            break;
        default:
            assert(0);
            break;
        }
    }
    if (optind > argc - 1) {
        help(EX_USAGE, stderr, argv[0]); /*NOTREACHED*/
    }
    unsigned long long start = strtoull(argv[optind], (char **)NULL, 0);
    unsigned long long length = (length_arg) ? strtoull(length_arg, (char **)NULL, 0) : 0;
    if (optind + 1 < argc) {
        writemem(start, length, argv + optind + 1, argc - (optind + 1));
    } else {
        char *files[1] = { "-" };
        writemem(start, length , files, 1);
    }
    exit(EX_OK); /*NOTREACHED*/
}
