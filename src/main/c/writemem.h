#ifndef WRITEMEM_H
#define WRITEMEM_H
/**
 * @file writemem.h
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief write memory.
 * @version 0.1.0
 * @date 2020-01-02
 * 
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include <stdio.h>

extern void help(int status, FILE *fp, const char *command);
extern FILE *fopen_or_die(const char *filename, const char *mode);
extern FILE *open_for_reading(const char *filename);
extern void fclose_or_die(FILE *fp);
extern void close_a_file(FILE *fp);
extern unsigned long long writemem(unsigned long long start, unsigned long long length, char *files[], size_t filesc);

#endif /* WRITEMEM_H.  */
