/**
 * @file fopen_or_die.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief open file or die.
 * @version 0.1.0
 * @date 2020-01-02
 * 
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include "writemem.h"
#include <err.h>
#include <sysexits.h>
#include <stdio.h>

/**
 * @brief open or die.
 * 
 * @param filename file.
 * @param mode mode.
 * @return FILE* opened.
 */
FILE *
fopen_or_die(const char *filename, const char *mode)
{
    FILE *fp = fopen(filename, mode);
    if (fp == NULL) {
        err(EX_NOINPUT, "%s: couldn't open", filename); /*NOTREACHED*/
    }
    return fp;
}
