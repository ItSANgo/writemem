/**
 * @file writemem_help.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief write memory - help.
 * @version 0.1.0
 * @date 2020-01-02
 * 
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include "writemem.h"
#include <stdlib.h>
#include <stdio.h>

/**
 * @brief --help function.
 * 
 * @param status exit status.
 * @param fp output.
 * @param command name.
 */
void
help(int status, FILE *fp, const char *command)
{
    fprintf(fp, "usage: %s [--help][--length length] address file...\n", command);
    exit(status); /*NOTREACHED*/
}
