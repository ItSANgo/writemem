/**
 * @file writemem.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief write memory.
 * @version 0.1.0
 * @date 2020-01-02
 * 
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include "writemem.h"

/**
 * @brief write to memory.
 * 
 * @param start address.
 * @param length 
 * @param files file list.
 * @param filesc number of files.
 * @return unsigned long long write length.
 */
unsigned long long
writemem(unsigned long long start, unsigned long long length, char *files[], size_t filesc)
{
    unsigned long long current_position = 0;
    size_t number_of_files;
    for (number_of_files = 0; number_of_files < filesc; ++number_of_files) {
        FILE *fp = open_for_reading(files[number_of_files]);
        int c;
        while ((c = fgetc(fp)) >= 0) {
           *(unsigned char *)(start + current_position) = c;
           ++current_position;
           if (length && current_position > length) {
               return current_position;
           }
        }
        close_a_file(fp);
    }
    return current_position;
}
