/**
 * @file close_a_file.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief close file or die.
 * @version 0.1.0
 * @date 2020-01-02
 * 
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include "writemem.h"
#include <stdio.h>

/**
 * @brief close a file.
 * 
 * @param fp close target.
 */
void
close_a_file(FILE *fp)
{
    if (fp == stdin) {
        return;
    }
    return fclose_or_die(fp);
}
