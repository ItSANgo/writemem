/**
 * @file open_for_reading.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief open for reading.
 * @version 0.1.0
 * @date 2020-01-02
 * 
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include "writemem.h"
#include <string.h>

/**
 * @brief open for reading.
 * 
 * @param filename file name.
 * @return FILE* opened.
 */
FILE *
open_for_reading(const char *filename)
{
    if (strcmp(filename, "-") == 0) {
        return stdin;
    }
    return fopen_or_die(filename, "r");
}