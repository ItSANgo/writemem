/**
 * @file fclose_or_die.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief close file or die.
 * @version 0.1.0
 * @date 2020-01-02
 * 
 * @copyright Copyright (c) 2020 Mitsutoshi Nakano
 * 
 */

#include "writemem.h"
#include <err.h>
#include <sysexits.h>
#include <stdio.h>

/**
 * @brief close or die.
 * 
 * @param fp close target.
 */
void
fclose_or_die(FILE *fp) {
    if (fclose(fp)) {
        err(EX_OSERR, "fclose():");
    }
}
